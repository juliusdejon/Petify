const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProfileInput(data) {
  let errors = {};

  data.handle = !isEmpty(data.handle) ? data.handle : '';

  if (!Validator.isLength(data.handle, { min: 2, max: 40 })) {
    errors.handle = 'Handle need to be between 2 and 40';
  }

  if (Validator.isEmpty(data.handle)) {
    errors.handle = 'handle field is required';
  }


  return {
    errors,
    isValid: isEmpty(errors)
  };
};
