const express = require('express');
const router = express.Router();
const multer = require('multer');
const crypto = require('crypto');
const mongoose = require('mongoose');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');
const path = require('path');
// Database Configuration
const db = require('../../config/keys').mongoURI;




// Create mongo connection

const conn = mongoose.createConnection(db);

//init gfs

let gfs;

conn.once('open', () => {
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection('uploads');
})

// Create storage engine

const storage = new GridFsStorage({
  url: db,
  file: (req, file) => {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const filename = buf.toString('hex') + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          bucketName: 'uploads'
        };
        resolve(fileInfo);
      });
    });
  }
});

const upload = multer({ storage });




// @route   GET api/posts/test
// @desc    Tests profile route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Posts Works' }));

// @route   GET api/posts/files 
// @desc    Display Files in json
// @access  Public
router.get('/files', (req, res) => {
  gfs.files.find().toArray((err, files) => {
    // Check if files 
    if (!files || files.length === 0) {
      return res.status(404).json({
        err: 'No files exist'
      });
    }

    // Files exist
    return res.json(files);
  })
});

// @route   GET api/posts/files/:filename
// @desc    Display Specific File
// @access  Public
router.get('/files/:filename', (req, res) => {
  gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
    // Check if file 
    if (!file || file.length === 0) {
      return res.status(404).json({
        err: 'No file exist'
      });
    }
    // File exists
    return res.json(file);
  })
});
// @route   GET api/posts/image/:filename
// @desc    Get Image
// @access  Public
router.get('/image/:filename', (req, res) => {
  gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
    // Check if file 
    if (!file || file.length === 0) {
      return res.status(404).json({
        err: 'No file exist'
      });
    }

    // Check if image
    if (file.contentType === 'image/png' || file.contentType === 'image/jpeg') {
      // Read output to browser
      const readstream = gfs.createReadStream(file.filename);
      readstream.pipe(res)
    } else {
      res.status(404).json({ err: 'Not an image' });
    }
  });
});



// @route   Post api/posts/
// @desc    Uploads file to DB
// @access  Public
router.post('/', upload.single('file'), (req, res) => {

  const postFields = req.formInfo;

  // Save Post
  new Profile(postFields).save().then(post => res.json(post));
});


// @route   GET api/posts/uploads/:id
// @desc    Get Image
// @access  Public

router.get('/uploads/:id', (req, res) => {
  Image.findById({ _id: req.params.id })
    .then(image => {
      res.contentType(image.contentType);
      res.send(image.data.data);
    })
    .catch(err => res.json(err));
});


module.exports = router;


