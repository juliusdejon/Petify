const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const PostSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  image: {.
    type: Schema.Types.ObjectId,
    ref: 'image'
  },
  title: {
    type: String,
    required: true
  },
  descripton: {
    type: String,
  },
  tyopeofAnimal: {
    type: String,
  },
  location: {
    type: String,
  },
  price: {
    type: int,
    required: true
  },
  sellerDetails: [{
    mobile: {

    },
    name: {

    },
  }],
  comments: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
      },
      text: {
        type: String,
        required: true
      },
      date: {
        type: Date,
        default: Date.now
      }
    }

  ],
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Post = mongoose.model('post', PostSchema);

