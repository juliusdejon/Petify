const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const ProfileSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  handle: {
    type: String,
    required: true,
    max: 40
  },
  firstName: {
    type: String,
    required: true,
    max: 40
  },
  lastName: {
    type: String,
    required: true,
    max: 40
  },
  contact: {
    type: String,
    required: true,
    max: 13
  },
  aboutMe: {
    type: String,
    max: 255,
  }
});

module.exports = Profile = mongoose.model('profile', ProfileSchema);
