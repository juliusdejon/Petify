import React from 'react';
import { Dimmer, Loader} from 'semantic-ui-react';

export default () => {
  return (
    <div>
       <Dimmer active inverted>
        <Loader inverted content='Loading' />
      </Dimmer>
    </div>
  );
};
