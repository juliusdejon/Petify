import React from 'react'
import { Image } from 'semantic-ui-react';

class Upload extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      file: null
    }
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange(event) {
    this.setState({
      file: URL.createObjectURL(event.target.files[0])
    })
  }
  render() {
    return (
      <div>
        <input type="file" onChange={this.handleChange} />
        <Image src={this.state.file} size='medium' centered />
      </div>
    );
  }
}
export default Upload;