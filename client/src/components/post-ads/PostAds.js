import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Header, Image, Divider, Segment, Form, Button, Select, Grid, GridColumn } from 'semantic-ui-react';
import { postAds } from '../../actions/postActions';
import defaultImage from '../../images/defaultImage.png';
class PostAds extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: '',
      title: '',
      price: 0,
      gender: 'male',
      description: '',
      name: '',
      email: '',
      contact: 0,
      errors: {},
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    // const formData = new FormData()
    // formData.append('file', this.state.file, this.state.file.name)
    // axios.post('/api/posts', formData)

    const formInfo = {
      title: this.state.title,
      price: this.state.price,
      gender: this.state.gender,
      description: this.state.description,
      name: this.state.name,
      contact: this.state.contact,
      email: this.state.email
    }
    this.props.postAds(formInfo, this.props.history);
  }


  handleImageChange = (e) => {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }
    if (file) {
      reader.readAsDataURL(file);
    }
  }


  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors })
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<Image src={imagePreviewUrl} size="small" bordered rounded centered onChange={this.handleImageChange} />);
    } else {
      $imagePreview = (<Image src={defaultImage} size="small" bordered rounded centered onChange={this.handleImageChange} />);
    }
    const options = [
      { key: 'm', text: 'Male', value: 'male' },
      { key: 'f', text: 'Female', value: 'female' },
    ]
    return (
      <div className="ui container">
        <Divider hidden />
        <Segment>
          <Header size="huge">Post Ads</Header>
          <Form onSubmit={this.handleSubmit} error loading={this.state.loading} encType="multipart/form-data">
            <Grid stackable columns={2}>
              <Grid.Column>
                <Form.Group>
                  <div className="image-upload" style={{ margin: 'auto' }}>
                    <label htmlFor="file-input">
                      {$imagePreview}
                    </label>
                    <input id="file-input" type="file" name="file" onChange={this.handleImageChange} hidden />
                  </div>
                </Form.Group>
                <Form.Group>
                  <Form.Input
                    label="Title"
                    placeholder='Title'
                    name='title'
                    value={this.state.title}
                    onChange={this.handleChange}
                    error={errors.title ? true : false}
                    size="tiny"
                    width={12}
                  />
                  <Form.Input
                    label="Price"
                    placeholder='Price'
                    name='price'
                    value={this.state.price}
                    onChange={this.handleChange}
                    error={errors.price ? true : false}
                    size="tiny"
                    width={4}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Field
                    control={Select}
                    label='Gender'
                    onChange={this.handleChange}
                    options={options}
                    value={this.state.gender}
                    placeholder='Gender' />
                </Form.Group>
                <Form.Group>
                  <Form.TextArea
                    label='Description'
                    placeholder='Description '
                    name='description'
                    onChange={this.handleChange}
                    value={this.state.description} width={16} />
                </Form.Group>
              </Grid.Column>
              <GridColumn>
                <label floated='left' className="ui teal button">Seller Details</label>
                <Divider clearing />
                <Form.Input
                  label="Name"
                  placeholder='Name'
                  name='name'
                  value={this.state.name}
                  onChange={this.handleChange}
                  error={errors.name ? true : false}
                  size="tiny"
                />
                <Form.Input
                  label="Contact Number"
                  placeholder='Contact'
                  name='contact'
                  value={this.state.contact}
                  onChange={this.handleChange}
                  error={errors.contact ? true : false}
                  size="tiny"
                />
                <Form.Input
                  label="Email Address"
                  placeholder='Email'
                  name='email'
                  value={this.state.email}
                  onChange={this.handleChange}
                  error={errors.email ? true : false}
                  size="tiny"
                />
              </GridColumn>
            </Grid>
            <Button type='submit'>Submit</Button>
          </Form>
          <div>
          </div>
        </Segment>
        <Divider hidden />
      </div>
    )
  }
}

PostAds.propTypes = {
  errors: PropTypes.object.isRequired,
  postAds: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  errors: state.errors
})
export default connect(mapStateToProps, { postAds })(withRouter(PostAds));