import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Header, Icon, Divider, Segment, Form, Button, TextArea, Grid, GridColumn } from 'semantic-ui-react';
import { createProfile } from '../../actions/profileActions';

class CreateProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            handle: '',
            firstName: '',
            lastName: '',
            contact: 0,
            aboutMe: '',
            errors: {},
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors })
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        const profileData = {
            handle: this.state.handle,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            contact: this.state.contact,
            aboutMe: this.state.aboutMe
        }
        console.log(profileData);

        this.props.createProfile(profileData, this.props.history);
    }
    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        const { errors } = this.state;

        return (
            <div className="ui container">
                <Divider hidden />
                <Segment>
                    <Header as='h2' icon textAlign='center' style={{ color: '#fe5960' }}>
                        <Icon name='user' circular />
                        <Header.Content>Create Profile</Header.Content>
                    </Header>
                    <Form onSubmit={this.handleSubmit} error loading={this.state.loading}>
                        <Grid stackable columns={2}>
                            <GridColumn>
                                <Form.Field>
                                    <label>@handle</label>
                                    <Form.Input
                                        placeholder='johndoe'
                                        name='handle'
                                        value={this.state.handle}
                                        onChange={this.handleChange}
                                        error={errors.handle ? true : false}
                                    />
                                </Form.Field>
                            </GridColumn>
                            <GridColumn>
                                <Form.Field>
                                    <label>contact</label>
                                    <Form.Input
                                        placeholder='+63'
                                        name='contact'
                                        value={this.state.contact}
                                        onChange={this.handleChange}
                                        error={errors.contact ? true : false}
                                    />
                                </Form.Field>
                            </GridColumn>
                        </Grid>
                        <Grid stackable columns={2}>
                            <GridColumn>
                                <Form.Field>
                                    <label>First Name</label>
                                    <Form.Input
                                        placeholder='johndoe'
                                        name='firstName'
                                        value={this.state.firstName}
                                        onChange={this.handleChange}
                                        error={errors.firstName ? true : false}
                                    />
                                </Form.Field>
                            </GridColumn>
                            <GridColumn>
                                <Form.Field>
                                    <label>Last Name</label>
                                    <Form.Input
                                        placeholder='johndoe'
                                        name='lastName'
                                        value={this.state.lastName}
                                        onChange={this.handleChange}
                                        error={errors.lastName ? true : false}
                                    />
                                </Form.Field>
                            </GridColumn>
                        </Grid>
                        <Grid>
                            <GridColumn>
                                <Form.Field
                                    control={TextArea}
                                    name="aboutMe"
                                    value={this.state.aboutMe}
                                    onChange={this.handleChange}
                                    label='About Me'
                                    placeholder='Tell us more about you...'
                                />
                            </GridColumn>
                        </Grid>
                        <Grid>
                            <GridColumn>
                                <Button type='submit' color="teal">Submit</Button>
                            </GridColumn>
                        </Grid>
                    </Form>
                </Segment>
            </div>
        )
    }
}

CreateProfile.propTypes = {
    profile: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    createProfile: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    profile: state.profile,
    errors: state.errors
})
export default connect(mapStateToProps, { createProfile })(withRouter(CreateProfile));