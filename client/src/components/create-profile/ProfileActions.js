import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Icon } from 'semantic-ui-react';

const ProfileActions = () => {
    return (
        <div>
            <Button.Group size='large'>
                <Link to='/edit-profile'>
                    <Button icon labelPosition='left'>
                        <Icon name='edit' />
                        Edit Profile
                </Button>
                </Link>
                <Button.Or />
                <Link to='/create-ads'>
                    <Button icon labelPosition='right'>
                        <Icon name="paw" />
                        Post Ads
            </Button></Link>
            </Button.Group>
        </div >
    )
}

export default ProfileActions;