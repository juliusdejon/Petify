import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { Image, Button } from 'semantic-ui-react'
import logo from '../../images/logo.png';
import avatar from '../../images/matthew.png';

import { logoutUser } from '../../actions/authActions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


class Navbar extends Component {
  render() {
    const {isAuthenticated } = this.props.auth;
    let NavType;
    if (isAuthenticated === true) {
      NavType = (<div>
      <div className="ui stackable center aligned segment secondary pointing menu">
        <Link to='/dashboard' className="item" ><img src={logo} alt="logo" /></Link>
        <a className="item">
          <div className="ui transparent left icon input">
            <input type="text" placeholder="Search..." />
            <i className="search icon"></i>
          </div>
        </a>
        <div className="right menu">
          <span className="ui item">
            <Image src={avatar} avatar />
            <span>{this.props.auth.user.name}</span>
          </span>
          <span className="ui item" onClick={() => this.props.logoutUser()}>
            <Button color="red">Logout</Button>
          </span>
        </div>
      </div>
    </div >)
    } else {
      NavType = (
      <div className="ui inverted vertical masthead center aligned segment">
      <div className="ui container">
        <div className="ui large secondary inverted pointing menu">
          <a className="toc item">
            <i className="sidebar icon"></i>
          </a>
          <Link to="/" className="active item">Home</Link>
          <div className="right item">
            <Link to='/register' className="ui inverted button">Sign Up</Link>
            <Link to='/login' className="ui inverted button">Log in</Link>
          </div>
        </div>
      </div>

      <div className="ui text container">
        <h1 className="ui inverted header">
          Petify
    </h1>
        <h2>Search for Pets</h2>
        <div className="ui fluid big icon input">
          <input type="text" placeholder="" />
          <i className="search icon"></i>
        </div>
      </div>
    </div>)
    }
    return (
      <div>
        {NavType}
        </div>
      )
  }
}
Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
}
const mapStateToProps = (state) => ({
  auth: state.auth,

});


export default connect(mapStateToProps, ({ logoutUser }))(Navbar);
