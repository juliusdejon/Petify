import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getCurrentProfile } from '../../actions/profileActions';
import { Segment, Divider, Header, Image } from 'semantic-ui-react';
import Spinner from '../commons/Spinner';
import avatar from '../../images/matthew.png';
import ProfileActions from '../create-profile/ProfileActions';

class Dashboard extends Component {
  componentDidMount() {
    this.props.getCurrentProfile();
  }

  render() {
    const { user } = this.props.auth;
    const { profile, loading } = this.props.profile;

    let dashboardContent;

    if (profile === null || loading) {
      dashboardContent = <Spinner />
    } else {
      // Check if logged in user has profile data
      if (Object.keys(profile).length > 0) {
        dashboardContent = (
          <div className="ui container">
            <Divider hidden />
            <Segment >
              <Header size='huge'>Dashboard</Header>
              <Header as='h2' size='huge'>
                <Image circular src={avatar} />
                Welcome, <Link to={`/profiles/${profile.handle}`}>{profile.firstName} {profile.lastName}</Link>!
  </Header>
              <ProfileActions />
            </Segment>
          </div>
        )
      } else {
        // User is logged in but has no profile
        dashboardContent = (
          <div className="ui container">
            <Divider hidden />
            <Segment >
              <Header size='huge'>Dashboard</Header>
              <Header as='h2'>
                <Image circular src={avatar} />
                Welcome, {user.name}!
              <p> You have not yet setup a profile, please add some info</p>
                <Link to="/create-profile">Create Profile</Link>
              </Header>
            </Segment>
          </div>
        )

      }
    }
    return (
      <div>
        {dashboardContent}
      </div>
    )
  }
}
Dashboard.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  profile: state.profile,
  auth: state.auth
});

export default connect(mapStateToProps, ({ getCurrentProfile }))(Dashboard);

