import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loginUser } from '../../actions/authActions';
import { Button, Form, Label, Divider } from 'semantic-ui-react'

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      errors: {},
      loading: false,
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard')
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors, loading: false });
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password,
    }
    this.props.loginUser(userData);
    this.setState({ loading: true })
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  render() {
    const { errors } = this.state;
    return (
      <div>
        <Divider hidden />
        <div className="ui raised very padded text container segment">
          <div className="ui top attached label">Login Form</div>
          <Form onSubmit={this.handleSubmit} error loading={this.state.loading}>
            <Form.Field>
              <label>Email</label>
              <Form.Input
                placeholder='Email'
                name='email'
                value={this.state.email}
                onChange={this.handleChange}
                error={errors.email ? true : false}
              />
            </Form.Field>
            {errors.email && (
              <Label basic color='red' pointing>{errors.email}</Label>
            )}
            <Form.Field>
              <label>Password</label>
              <Form.Input
                placeholder='Password'
                name='password'
                type="password"
                value={this.state.password}
                onChange={this.handleChange}
                error={errors.password ? true : false}
              />
              {errors.password && (
                <Label basic color='red' pointing>{errors.password}</Label>
              )}
            </Form.Field>
            <Button type='submit'>Submit</Button>
          </Form>
          </div>
        </div>
    )
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
}
const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(mapStateToProps, { loginUser })(Login);
