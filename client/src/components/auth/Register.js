import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import { registerUser } from '../../actions/authActions';
import { Button, Form, Label, Divider } from 'semantic-ui-react';

class Register extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      password2: '',
      errors: {},
      loading: false
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();
    const newUser = {
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2
    }
    this.props.registerUser(newUser, this.props.history);
    this.setState({ loading: true })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors, loading: false })
    }
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  render() {

    const { errors } = this.state;

    return (
      <div>
        <Divider hidden />
        <div className="ui raised very padded text container segment">
          <div className="ui top attached label">Registration Form</div>
          <Form onSubmit={this.handleSubmit} error loading={this.state.loading}>
            <Form.Field>
              <label>Email</label>
              <Form.Input
                placeholder='Email'
                name='email'
                value={this.state.email}
                onChange={this.handleChange}
                error={errors.email ? true : false}
              />
            </Form.Field>
            {errors.email && (
              <Label basic color='red' pointing>{errors.email}</Label>
            )}
            <Form.Field>
              <label>Password</label>
              <Form.Input
                placeholder='Password'
                name='password'
                type="password"
                value={this.state.password}
                onChange={this.handleChange}
                error={errors.password ? true : false}
              />
              {errors.password && (
                <Label basic color='red' pointing>{errors.password}</Label>
              )}
            </Form.Field>
            <Form.Field>
              <label>Confirm Password</label>
              <Form.Input
                placeholder='Password'
                name='password2'
                type="password"
                value={this.state.password2}
                onChange={this.handleChange}
                error={errors.password2 ? true : false}
              />
              {errors.password && (
                <Label basic color='red' pointing>{errors.password}</Label>
              )}
            </Form.Field>
            <Button type='submit'>Submit</Button>
          </Form>
        </div>
      </div >

    )
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  errors: state.errors
});

export default connect(mapStateToProps, { registerUser })(withRouter(Register));
