import React from 'react';
import { Segment,Header } from 'semantic-ui-react';

export const Notification = (props) => {
  return (
   
      <Segment style={{ left: '40%', position: 'fixed', top: '50%', zIndex: 1000 }}>
        <Header>This is an example portal</Header>
        <p>Portals have tons of great callback functions to hook into.</p>
        <p>To close, simply click the close button or click away</p>
      </Segment>
   
  )
}