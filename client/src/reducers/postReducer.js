import { GET_POST, POST_LOADING } from '../actions/types';


const initialState = {
  post: null,
  posts: null,
  loading: false
}


export default function (state = initialState, action) {
  switch (action.type) {
    case POST_LOADING:
      return {
        ...state,
        loading: true
      }
    case GET_POST:
      return {
        ...state,
        profile: action.payload,
        loading: false
      };
    default:
      return state;
  }
}