import axios from 'axios';

import { GET_ERRORS, POST_LOADING } from './types';


// Create POST
export const postAds = (postData, history) => dispatch => {
  axios
    .post('/api/post', postData)
    .then(res => history.push('/dashboard'))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    )
}

// Post Loading
export const setPostLoading = () => {
  return {
    type: POST_LOADING
  }
}
